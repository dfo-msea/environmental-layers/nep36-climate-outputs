import os
import traceback
from osgeo import gdal, osr
import glob
import logging
from datetime import datetime
import sys

# Python bindings do not raise exceptions unless you explicitly call UseExceptions()
gdal.UseExceptions()

# set up global variables
# date string
DATE_TIME = datetime.now().strftime("%Y-%m-%d")
# log name
LOG_NAME = "NEP36-CanOE"
# log string
LOG_STRING = f"LOGGING {LOG_NAME} - {DATE_TIME}"
FILE_FORMAT = "GTiff"
DRIVER = gdal.GetDriverByName(FILE_FORMAT)


def setup_logger(directory, name=LOG_NAME, date=DATE_TIME, level=logging.INFO):
    # Gets or creates a logger
    logger = logging.getLogger(name)

    # set log level
    logger.setLevel(level)

    # define file handler and set formatter
    file_handler = logging.FileHandler(os.path.join(directory, f"{name}-{date}.log"))
    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(module)s(%(lineno)d) - %(message)s")
    file_handler.setFormatter(formatter)

    # add file handler to logger
    logger.addHandler(file_handler)
    return logger


def get_rasters(directory, logger):
    try:
        logger.info(f"Searching for GeoTIFF files...")
        geotiffs = glob.glob(f"{directory}/*.tif")
        logger.info(f"{len(geotiffs)} GeoTIFF files found...")
        return geotiffs
    except Exception:
        logger.error(traceback.format_exc())


def check_dir(input_dir):
    try:
        if os.path.exists(input_dir):
            print(f"Input directory: {input_dir}")
            return input_dir
        else:
            print(f"{input_dir} does not exist. Exiting program.")
            sys.exit()
    except Exception:
        logging.error(traceback.format_exc())


def make_dir(output_path):
    try:
        out_name = f"{LOG_NAME}"
        # Make output folder
        outpath = os.path.join(output_path, out_name)
        # Make output directory if it does not exist already
        if not os.path.exists(outpath):
            print(f"Creating output directory: {outpath}")
            os.mkdir(outpath)
        else:
            print(f"{outpath} already exists.")
        return outpath
    except Exception:
        logging.error(traceback.format_exc())


def make_subdir(input_dir, output_dir):
    # get base name of directory from input
    basename = os.path.basename(input_dir)
    try:
        # output path
        outpath = os.path.join(output_dir, basename)
        # Make output directory if it does not exist already
        if not os.path.exists(outpath):
            print(f"Creating output subdirectory: {outpath}")
            os.mkdir(outpath)
        else:
            print(f"{outpath} already exists.")
        return outpath
    except Exception:
        logging.error(traceback.format_exc())


def copy_raster(ras_path, ras_name, output_dir, logger):
    try:
        logger.info(f" * Creating copy of {ras_name}")
        # open raster
        in_ras = gdal.Open(ras_path)
        # create filename for output
        dst_filename = os.path.join(output_dir, ras_name)
        # create copy and return
        out_ras = DRIVER.CreateCopy(dst_filename, in_ras)
        return out_ras
    except Exception:
        logger.error(traceback.format_exc())


def get_band(ras, logger):
    try:
        logger.info(" * Accessing band 1...")
        b1 = ras.GetRasterBand(1)
        return b1
    except Exception:
        logger.error(traceback.format_exc())


def assign_nodata(band, nodata_val, logger):
    try:
        nodata = float(nodata_val)
        logger.info(f" * Assigning NoData value as {nodata}...")
        band.SetNoDataValue(nodata)
        return band
    except Exception:
        logger.error(traceback.format_exc())


def report_srs(ras, logger):
    try:
        logger.info(" * Reading Spatial Reference System Information...")
        prj = ras.GetProjection()
        logger.info(f" >> {prj}")
        srs = osr.SpatialReference(wkt=prj)
        if srs.GetAttrValue('projcs') is not None:
            logger.info(f" >> Projected Coordinate System: {srs.GetAttrValue('projcs')}")
        else:
            logger.info(f" >> Geographic Coordinate System: {srs.GetAttrValue('geogcs')}")
        return srs
    except Exception:
        logger.error(traceback.format_exc())


def calc_stats(ras, logger):
    try:
        logger.info(" * Calculating statistics for raster layer...")
        gdal.Info(ras, stats=True)
        return ras
    except RuntimeError:
        logger.error("Failed to compute statistics, no valid pixels found in sampling.")
    except Exception:
        logger.error(traceback.format_exc())


def build_overview(ras, logger):
    try:
        logger.info(" * Building overviews for raster layer...")
        ras.BuildOverviews("NEAREST", [2, 4, 8, 16, 32, 64])
        return ras
    except AttributeError:
        logger.error("'NoneType' object has no attribute 'BuildOverviews'")
    except Exception:
        logger.error(traceback.format_exc())


def process_raster(raster_list, output_dir, nodata_value, logger):
    num_files = len(raster_list)
    for index, ras in enumerate(raster_list):
        # Get basename of raster file.
        ras_name = os.path.basename(ras)
        logger.info("-" * len(ras_name))
        logger.info(f"Processing {ras_name}: file {index + 1} of {num_files}...")
        new_ras = copy_raster(ras, ras_name, output_dir, logger)
        band1 = get_band(new_ras, logger)
        band1 = assign_nodata(band1, nodata_value, logger)
        spatial_ref = report_srs(new_ras, logger)
        new_ras = calc_stats(new_ras, logger)
        new_ras = build_overview(new_ras, logger)
        logger.info(f" * Dereferencing {ras_name} and saving to disk in {output_dir}...")
        band1 = None  # dereference band to allow for saving to disk
        new_ras = None  # save, close
    return raster_list


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("input_dir", type=str,
                        help="Full path to input directory where data are located.")
    parser.add_argument("output_dir", type=str,
                        help="Full path to output directory for processed data.")
    parser.add_argument("nodata_value", type=str,
                        help="Value to use for NoData values.")
    args = parser.parse_args()
    dir = make_dir(args.output_dir)
    sub_dir = make_subdir(args.input_dir, dir)
    logger = setup_logger(sub_dir)
    logger.info("-" * len(LOG_STRING))
    logger.info(LOG_STRING)
    logger.info("-" * len(LOG_STRING))
    # log params
    logger.info("PARAMETERS")
    logger.info(f"Input path: {args.input_dir}")
    logger.info("-" * len(LOG_STRING))
    # run program
    raster_files = get_rasters(args.input_dir, logger)
    raster_files_updated = process_raster(raster_files, sub_dir, args.nodata_value, logger)
    logger.info("PROCESSING: COMPLETE")


if __name__ == "__main__":
    main()
