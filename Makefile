default: NEP36-CanOE-historical.zip NEP36-CanOE-RCP45.zip NEP36-CanOE-RCP85.zip

NEP36-CanOE_geotiffs/historical: extract_layers.py
	python extract_layers.py --simulations historical_1986-2005 --output_dir NEP36-CanOE_geotiffs/historical

NEP36-CanOE_geotiffs/RCP45: extract_layers.py
	python extract_layers.py --simulations RCP45_2046-2065 --output_dir NEP36-CanOE_geotiffs/RCP45

NEP36-CanOE_geotiffs/RCP85: extract_layers.py
	python extract_layers.py --simulations RCP85_2046-2065 --output_dir NEP36-CanOE_geotiffs/RCP85

NEP36-CanOE-historical.zip: NEP36-CanOE_geotiffs/historical
	mkdir -p NEP36-CanOE/historical
	python process_tiffs.py NEP36-CanOE_geotiffs/historical . -9999
	zip NEP36-CanOE-historical.zip NEP36-CanOE/historical/*

NEP36-CanOE-RCP45.zip: NEP36-CanOE_geotiffs/RCP45
	mkdir -p NEP36-CanOE/RCP45
	python process_tiffs.py NEP36-CanOE_geotiffs/RCP45 . -9999
	zip NEP36-CanOE-RCP45.zip NEP36-CanOE/RCP45/*

NEP36-CanOE-RCP85.zip: NEP36-CanOE_geotiffs/RCP85
	mkdir -p NEP36-CanOE/RCP85
	python process_tiffs.py NEP36-CanOE_geotiffs/RCP85 . -9999
	zip NEP36-CanOE-RCP85.zip NEP36-CanOE/RCP85/*

.PHONY: clean
clean:
	rm -r NEP36-CanOE
	rm -f NEP36-CanOE-historical.zip NEP36-CanOE-RCP45.zip NEP36-CanOE-RCP85.zip

.PHONY: deepclean
deepclean: clean
	rm -r NEP36-CanOE_geotiffs
