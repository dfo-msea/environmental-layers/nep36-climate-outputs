import os
import rioxarray
import xarray as xr
import numpy as np
import argparse
import shutil


def process_climate_output(simulation, 
                           variable,
                           input_dir = './NEP36-CanOE_masked_715',
                           output_dir = '.',
                           ncfile_pattern = 'NEP36-CanOE_{variable}_{simulation}_monthly_715x715_mask.nc'
                        ):

    print(f'Processing {simulation} for {variable}...')

    simulation_shortname = simulation.split('_')[0]

    path = os.path.join(input_dir, simulation_shortname)
    ncfile = os.path.join(path, ncfile_pattern.format(
        variable=variable, simulation=simulation))

    ds = xr.open_dataset(ncfile)

    if 'deptht' in ds:
        # use the same name for depth dimension
        ds = ds.rename({'deptht': 'depth'})

    darr = ds[variable]

    # handle the special case of integrated primary production
    if variable == 'TPP':
        variable = 'ipp'
        darr.name = 'ipp'
        
        # create a mask for the ocean
        ocean_mask = np.ones((darr.sizes['lat'], darr.sizes['lon'])) * np.isfinite(darr.isel(t=0, depth=0))
        
        # replace NaNs with 0 (should not affect integration)
        darr = darr.fillna(0)
        
        # integrate vertically with trapezoid rule
        darr = darr.integrate('depth')
        
        # apply the ocean mask again
        darr = darr.where(ocean_mask==1)

        # change units from mol/m2/s to mol/m2/Yr
        darr = darr*60*60*24*365

    darr['t'] = ['fallwinter', 'fallwinter', 'fallwinter',       # JFM
                 'springsummer', 'springsummer', 'springsummer', # AMJ
                 'springsummer', 'springsummer', 'springsummer', # JAS
                 'fallwinter', 'fallwinter', 'fallwinter'        # OND
                 ]

    darr = darr.reindex(lat=darr.lat[::-1])
    
    def average_and_save_geotiff(darr_level, outfile_pattern):
        """give a 2d dataarray save it as a geotiff for each season"""

        for season in ['springsummer', 'fallwinter']:
            darr_mean = darr_level.groupby('t').mean('t')

            darr_output = darr_mean.sel(t=season)
            darr_output.fillna(-9999)
            darr_output.rio.set_nodata(-9999, inplace=True)
            darr_output.rio.set_crs('EPSG:4326', inplace=True)

            filename = outfile_pattern.format(season=season)
            darr_output.rio.to_raster(os.path.join(output_dir, filename))

    if variable in ['mld', 'ipp']:
        # this is a 2d variable
        outfile_pattern = f'NEP36-CanOE_{simulation}_{variable}_{{season}}.tif'

        average_and_save_geotiff(darr, outfile_pattern)

    else:
        # this is a 3d variable
        for level in ['surface', 'bottom']:

            outfile_pattern = f'NEP36-CanOE_{simulation}_{variable}_{{season}}_{level}.tif'

            if level == 'surface':
                depth_index = 0
            else:  # find index of the "bottom"
                depth_index = darr.notnull().sum('depth') - 1

            darr_level = darr.isel(depth=depth_index)

            average_and_save_geotiff(darr_level, outfile_pattern)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str,
                        default='./NEP36-CanOE_masked_715',
                        help="input directory where data are located.")
    parser.add_argument("--ncfile_pattern", type=str,
                        default='NEP36-CanOE_{variable}_{simulation}_monthly_715x715_mask.nc',
                        help="filename pattern for input data")
    parser.add_argument("--output_dir", type=str,
                        default='.',
                        help="output directory for processed data.")
    parser.add_argument("--variables", type=str, nargs='+',
                        default=['mld', 'TPP', 'O2', 'PH', 'temp', 'salt', 'NO3', 'speed'],
                        help="variables to process (defaults to all)")
    parser.add_argument("--simulations", type=str, nargs='+',
                        default= ['historical_1986-2005', 'RCP45_2046-2065', 'RCP85_2046-2065'],
                        help="simulations to process (defaults to all)")
    args = parser.parse_args()

    os.makedirs(args.output_dir, exist_ok=True)

    # iterate through all combinations of simulations and variables
    for variable in args.variables:
        for simulation in args.simulations:
            process_climate_output(simulation=simulation, 
                                   variable=variable,
                                   input_dir=args.input_dir,
                                   output_dir=args.output_dir,
                                   ncfile_pattern=args.ncfile_pattern
                                   )

    
